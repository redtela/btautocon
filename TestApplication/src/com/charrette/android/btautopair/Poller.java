package com.charrette.android.btautopair;

import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

public class Poller implements Runnable {

	private boolean isRunning = false;
	private boolean isBonded = false;

	private int interval = 0;
	private String deviceName;
	private String pin;
	private static BluetoothAdapter bluetooth;

	private BluetoothDevice device;

	public Poller(int i, String device, String pin, BluetoothAdapter adaptor) {
		this.interval = i;
		this.deviceName = device;
		this.pin = pin;
		bluetooth = adaptor;
		this.isRunning = true;
		Log.i("Poller", "Initialised.");
	}

	private byte[] getPinAsBytes() {
		return this.pin.getBytes(Charset.forName("UTF-8"));
	}

	private void connect(BluetoothDevice device) {
		if (isBonded) return;

		Log.d("Pairing", "Starting pairing...");
		try {
			device.setPin(getPinAsBytes());
			device.createBond();
			Log.d("Pairing", "Pairing complete.");
			this.isBonded = true;
		} catch (Exception e) {
			Log.e("Pairing", "Exception when pairing", e);
		}
	}

	@SuppressWarnings("unused")
	private void disconnect() {
		try {
			Method m = device.getClass().getMethod("removeBond", (Class[]) null);
			m.invoke(device, (Object[]) null);
		} catch (Exception e) {
			Log.e("Poller", e.getMessage());
		}
	}

	private void fireDeviceFound( BluetoothDevice device ) {
		StringBuilder buf = new StringBuilder("DeviceFound!\r\n");
		buf.append( "Name: " + device.getName() + "\r\n");
		buf.append( "Address: " + device.getAddress() + "\r\n");
		Log.i("Poller", buf.toString());

		if (this.device == null) synchronized (device) {
			connect( device );
		}
	}

	@Override
	public void run() {
		while (this.isRunning) {
			try {
				Thread.sleep( this.interval );
			} catch (Exception e) { /* interrupted */ }
			if (!this.isBonded) {
				Log.d("Poller", "Polling...");
				Set<BluetoothDevice> devices = bluetooth.getBondedDevices();
				Log.d("Poller", "Found " + devices.size() + " devices.");
				for (BluetoothDevice device : devices) {
					if (device.getName().equals(this.deviceName))
						fireDeviceFound( device );
				}
			}
		}
	}

}
