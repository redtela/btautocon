package com.charrette.android.btautopair;

import static com.charrette.android.btautopair.PairingRequest.PAIRING_REQUEST;
import static android.content.Context.MODE_PRIVATE;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

public class PairingService extends BroadcastReceiver {

	private static final String DEFAULT_DEVICE	= "Jaguar Audio";
	private static final String DEFAULT_PIN		= "0000";
	
	private static String device;
	private static String pin;
	
	@Override
	public void onReceive(Context ctx, Intent intent) {
		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			loadParameters(ctx);
			IntentFilter filter = new IntentFilter(PAIRING_REQUEST);
			ctx.registerReceiver(new PairingRequest(device, pin), filter);
		}
	}
	
	private void loadParameters(Context ctx) {
		SharedPreferences prefs = ctx.getSharedPreferences("BTAutoPrefs", MODE_PRIVATE);
		device = prefs.getString("device", DEFAULT_DEVICE);
		pin = prefs.getString("pin", DEFAULT_PIN);
	}
}
