package com.charrette.android.btautopair;

import java.nio.charset.Charset;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class PairingRequest extends BroadcastReceiver {

	public static final String PAIRING_REQUEST = "android.bluetooth.device.action.PAIRING_REQUEST";
	private String pin;
	private String deviceName;

	public PairingRequest(String deviceName, String pin) {
		super();
		this.deviceName = deviceName;
		this.pin = pin;
	}

	private byte[] getPinAsBytes() {
		return this.pin.getBytes(Charset.forName("UTF-8"));
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(PAIRING_REQUEST)) {
			try {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				if (device.getName().equals(this.deviceName)) {
					device.setPin(getPinAsBytes());
					device.setPairingConfirmation(true);
					device.createBond();
					Log.d("Pairing", "Pairing complete.");
					Toast.makeText(context, "Pairing complete.", Toast.LENGTH_LONG).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}