package com.charrette.android.btautopair;

import static com.charrette.android.btautopair.PairingRequest.PAIRING_REQUEST;

import com.charrette.android.testapplication.R;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	private static final BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();

	private ToggleButton toggleBluetooth;
	private EditText txtPollDelay;
	private EditText txtDeviceName;
	private EditText txtDevicePIN;

	private Poller poller;
	private Thread th;
	private boolean pollerEnabled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		txtPollDelay = (EditText) findViewById(R.id.txtPollInterval);
		txtDeviceName = (EditText) findViewById(R.id.txtDeviceName);
		txtDevicePIN = (EditText) findViewById(R.id.txtPIN);
		toggleBluetooth = (ToggleButton) findViewById(R.id.toggleBluetooth);
		toggleBluetooth.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				doBluetoothToggle( v );
			}
		});

		ToggleButton togglePoller = (ToggleButton) findViewById(R.id.togglePoller);
		togglePoller.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				doPollerToggle( v );
			}
		});

		Button btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				saveData();
			}
		});
		initialise();
		saveData();
		
		IntentFilter filter = new IntentFilter(PAIRING_REQUEST);
		PairingRequest request = new PairingRequest( 
				txtDeviceName.getText().toString(),
				txtDevicePIN.getText().toString());
		registerReceiver(request, filter);
	}

	private static void setBluetoothStatus(boolean value) {
		synchronized (bluetooth) { 
			try {
				if (value) {
					bluetooth.enable();
					Log.i("Main", "Bluetooth enabled.");
				}
				else {
					bluetooth.disable();
					Log.i("Main", "Bluetooth disabled.");
				}
			} catch (Exception e) {
				Log.e("Main", "Exception toggling bluetooth!", e);
			}
		}
	}

	private static boolean isBluetoothEnabled() {
		return (bluetooth != null && bluetooth.isEnabled());
	}

	private void initialise() {
		toggleBluetooth.setChecked( isBluetoothEnabled() );
	}

	private void doBluetoothToggle(View v) {
		setBluetoothStatus( !isBluetoothEnabled() );
		if (isBluetoothEnabled())
			startPoller();
	}

	private void doPollerToggle(View v) {
		if (!pollerEnabled)
			startPoller();
		else stopPoller();
	}

	private void startPoller() {
		Log.i("Main", "Starting Poller...");
		try {
			if (poller == null) {
				poller = new Poller(Integer.parseInt(txtPollDelay.getText().toString()),
						txtDeviceName.getText().toString(),
						txtDevicePIN.getText().toString(),
						bluetooth);
				th = new Thread( poller );
				th.start();
				Log.d("Main", "Poll thread started.");
			} else {
				Log.e("Main", "Poller wasn't null");
				// TODO: What if poller isn't null?
			}
		} catch (Exception e) {
			Log.e("Main", "Erorr starting poller.", e);
			// TODO: Exception handling
		}
	}

	private void stopPoller() {
		Log.w("Main", "Stop Poller called.");
		// TODO: Stop the poller
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void saveData() {		
		SharedPreferences sp = getSharedPreferences("BTAutoPrefs", Context.MODE_PRIVATE);
		Editor editor = sp.edit();   
		editor.putString("device", txtDeviceName.getText().toString());
		editor.putString("pin", txtDevicePIN.getText().toString());
		editor.commit();
	}
}
